<?php
///////////////////////////////////
// ./app/vues/template/index.php //
//////////////////////////////////
?>

 <!DOCTYPE html>
 <html lang="en">

   <head>
     <?php include '../app/vues/template/partials/head.php'; ?>
   </head>

   <body>
     <!-- Navigation -->
     <?php include '../app/vues/template/partials/nav.php'; ?>
     <!-- CONTENU -->
     <main>

         <!-- Page Header -->
         <header class="masthead" style="background-image: url('img/home-bg.jpg')">
           <div class="container">
             <div class="row">
               <div class="col-lg-8 col-md-10 mx-auto">
                 <div class="site-heading">
                   <h1>Clean Blog</h1>
                   <span class="subheading">A Blog Theme by Start Bootstrap</span>
                 </div>
               </div>
             </div>
           </div>
         </header>

         <!-- Textes -->
         <div class="container">
           <div class="row">
             <div class="col-lg-8 col-md-10 mx-auto">
               <div class="clearfix">
                 <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit placeat repudiandae maiores sequi nemo aliquid libero dolores ab debitis commodi, veniam fuga iste nulla impedit ea nam itaque optio, quibusdam.</p>
                 <p>Eveniet optio tempore beatae, nulla voluptatibus repudiandae, rem dolor, sunt neque perferendis pariatur. Placeat nobis explicabo, maxime soluta magnam ullam mollitia voluptatibus, quia in, autem molestias voluptatem ducimus fuga error.</p>
               </div>

               <!-- ADD A POST -->
               <div class="clearfix">
                 <a class="btn btn-secondary float-left" href="#">Add Post &rarr;</a>
               </div>

               <!-- POSTS LIST -->
               <div class="post-preview">
                 <a href="post.html">
                   <h2 class="post-title">
                     Man must explore, and this is exploration at its greatest
                   </h2>
                   <h3 class="post-subtitle">
                     Problems look mighty small from 150 miles up
                   </h3>
                 </a>
                 <p class="post-meta">Posted on September 24, 2017</p>
               </div>
               <hr>
               <div class="post-preview">
                 <a href="post.html">
                   <h2 class="post-title">
                     I believe every human has a finite number of heartbeats. I don't intend to waste any of mine.
                   </h2>
                 </a>
                 <p class="post-meta">Posted on September 18, 2017</p>
               </div>
               <hr>
               <div class="post-preview">
                 <a href="post.html">
                   <h2 class="post-title">
                     Science has not yet mastered prophecy
                   </h2>
                   <h3 class="post-subtitle">
                     We predict too much for the next year and yet far too little for the next ten.
                   </h3>
                 </a>
                 <p class="post-meta">Posted on August 24, 2017</p>
               </div>
               <hr>
               <div class="post-preview">
                 <a href="post.html">
                   <h2 class="post-title">
                     Failure is not an option
                   </h2>
                   <h3 class="post-subtitle">
                     Many say exploration is part of our destiny, but it’s actually our duty to future generations.
                   </h3>
                 </a>
                 <p class="post-meta">Posted on July 8, 2017</p>
               </div>
               <hr>
               <!-- Pager -->
               <div class="clearfix">
                 <a class="btn btn-secondary float-right" href="#">Older Posts &rarr;</a>
               </div>
             </div>
           </div>
         </div>

         <hr>
     </main>
     <!-- Footer -->
     <footer>
       <div class="container">
         <div class="row">
           <div class="col-lg-8 col-md-10 mx-auto">
             <ul class="list-inline text-center">
               <li class="list-inline-item">
                 <a href="#">
                   <span class="fa-stack fa-lg">
                     <i class="fa fa-circle fa-stack-2x"></i>
                     <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
                   </span>
                 </a>
               </li>
               <li class="list-inline-item">
                 <a href="#">
                   <span class="fa-stack fa-lg">
                     <i class="fa fa-circle fa-stack-2x"></i>
                     <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
                   </span>
                 </a>
               </li>
               <li class="list-inline-item">
                 <a href="#">
                   <span class="fa-stack fa-lg">
                     <i class="fa fa-circle fa-stack-2x"></i>
                     <i class="fa fa-github fa-stack-1x fa-inverse"></i>
                   </span>
                 </a>
               </li>
             </ul>
             <p class="copyright text-muted">Copyright &copy; Your Website 2017</p>
           </div>
         </div>
       </div>
     </footer>

     <!-- Bootstrap core JavaScript -->
     <script src="vendor/jquery/jquery.min.js"></script>
     <script src="vendor/popper/popper.min.js"></script>
     <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

     <!-- Custom scripts for this template -->
     <script src="js/clean-blog.min.js"></script>

   </body>

 </html>
